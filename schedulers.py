from diffusers import LMSDiscreteScheduler, DDIMScheduler, DDPMScheduler, PNDMScheduler, EulerDiscreteScheduler, EulerAncestralDiscreteScheduler, DPMSolverMultistepScheduler

def get_scheduler(name, config, cache):
    if name == 'lms':
        scheduler = LMSDiscreteScheduler
    elif name == 'ddim':
        scheduler = DDIMScheduler
    elif name == 'ddpm':
        scheduler = DDPMScheduler
    elif name == 'pndm':
        scheduler = PNDMScheduler
    elif name == 'euler':
        scheduler = EulerDiscreteScheduler
    elif name == 'euler_a':
        scheduler = EulerAncestralDiscreteScheduler
    elif name == 'heun':
        scheduler = HeunDiscreteScheduler
    elif name == 'dpm2':
        scheduler = DPMSolverSinglestepScheduler
    elif name == 'dpm':
        scheduler = DPMSolverMultistepScheduler
    else:
        scheduler = None

    if name in cache:
        instance = cache[name]
    else:
        instance = scheduler.from_config(config)
        cache[name] = instance
    return instance
