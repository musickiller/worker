from pipelines import build_pipeline
from diffusers.models import AutoencoderKL
from diffusers.utils.import_utils import is_xformers_available
import numpy as np
import diffusers
import io
import os
import requests
import time
import torch
import uuid
import gc
import codecs
import base64
import schedulers
import PIL

host = "https://www.xno.ai"
if "XNOAI_HOST" in os.environ:
    host = os.environ["XNOAI_HOST"]
device='cuda:0'

model_id = None
if "XNOAI_MODEL" in os.environ:
    model_id = os.environ["XNOAI_MODEL"]
model_type = "sd"
if "XNOAI_MODEL_TYPE" in os.environ:
    model_type = os.environ["XNOAI_MODEL_TYPE"]

if "XNOAI_DEBUG" not in os.environ:
    diffusers.logging.set_verbosity(diffusers.logging.ERROR)

features = ["variants", "save_embeddings", "samplers_v4", "sd2"]
enable_attention_slicing = True
enable_xformers_memory_efficient_attention = is_xformers_available()
if not enable_xformers_memory_efficient_attention:
    print("Error: xformers is now required.")
    print("""To install xformers:
    pip install -U git+https://github.com/facebookresearch/xformers.git@main#egg=xformers
    (this can take dozens of minutes).""")
    exit(1)
enable_everything = False
timeout = 10.0

if "XNOAI_DISABLE_ATTENTION_SLICING" in os.environ:
    enable_attention_slicing = False
if "XNOAI_DISABLE_MODEL_SWITCHING" not in os.environ:
    features += ["switch_models"]
if "XNOAI_DISABLE_INPAINTING" not in os.environ:
    features += ["inpainting"]
if "XNOAI_DISABLE_IMG2IMG" not in os.environ:
    features += ["img2img"]

if "XNOAI_ENABLE_EVERYTHING" in os.environ:
    enable_everything = True
if enable_everything or "XNOAI_ENABLE_HIGH_RESOLUTION" in os.environ:
    features += ["high_resolution"]
if enable_everything or "XNOAI_ENABLE_SELF_ATTENTION_GUIDANCE" in os.environ:
    features += ["self_attention_guidance"]
if enable_everything or "XNOAI_ENABLE_LARGE_MODELS1" in os.environ:
    features += ["lm1"]
vae = None
revision = None
if "XNOAI_REVISION" in os.environ:
    revision = os.environ["XNOAI_REVISION"]

props = torch.cuda.get_device_properties(device)
uid = uuid.uuid4().hex.upper()[0:8]
gb = str(int(props.total_memory//1024//1024//1024))+"G"+uid
worker_id = props.name+"-"+gb
api_key = ""
if "XNOAI_API_KEY" in os.environ:
    api_key = os.environ["XNOAI_API_KEY"]
headers={"X-Api-Key": api_key, "X-Worker-Id": worker_id}

while(True):
    try:
        response = requests.get(host+"/api/models/default.json", headers=headers, timeout=timeout)
        if response.status_code != 200:
            print("Error getting defaults from xno.ai. Retrying.", response.status_code)
            time.sleep(5)
            continue

        model_defaults = response.json()

        if model_id is None:
            model_id = model_defaults["model_id"]
            revision = model_defaults["revision"]
        vae = model_defaults["vae"]
    except(requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
        print("Error connecting to xno.ai, retrying in 5 seconds")
        time.sleep(5)
        continue
    break

pipe = build_pipeline(model_id, revision=revision, use_auth_token=os.environ["HF_AUTH_TOKEN"], vae=AutoencoderKL.from_pretrained(vae))
if enable_attention_slicing:
    pipe.enable_attention_slicing()
if enable_xformers_memory_efficient_attention:
    pipe.enable_xformers_memory_efficient_attention()
pipe = pipe.to(device)

scheduler_config = pipe.scheduler.config
scheduler_cache = {}

print("... Querying work pool ...")
print("Your GPU will only run when there is work to be done.")
while(True):
    url = host+"/runs"
    while(True):
        try:
            data = {"version": 11, "model_id": model_id, "features": ",".join(features)}
            run_response = requests.post(url, headers=headers, data=data, timeout=timeout)
        except(requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
            print("Error connecting to xno.ai, retrying in 5 seconds")
            time.sleep(5)
            continue
        break
    if run_response.status_code == 403:
        print("Invalid api key, "+url, "set XNOAI_API_KEY")
        exit(-1)
    if run_response.status_code != 200:
        print("error!", run_response.status_code)
        print(run_response)
        time.sleep(5)
        continue
    details = run_response.json()

    if len(details) == 0:
        time.sleep(1)
    for detail in details:
        if "switch_model" in detail:
            model_type = detail["model_type"]
            previous_model_id = model_id
            print("Switching the running model to '"+detail["model_id"])
            del pipe
            pipe = None
            torch.cuda.empty_cache()
            gc.collect()
            model_id = detail["model_id"]
            revision = None
            if "model_revision" in detail:
                revision = detail["model_revision"]

            try:
                vae = detail["vae"]
                pipe = build_pipeline(model_id, model_type=model_type, revision=revision, use_auth_token=os.environ["HF_AUTH_TOKEN"], vae=AutoencoderKL.from_pretrained(vae))
            except EnvironmentError as e:
                print("Received error while trying to load "+model_id+". Please accept the terms located here: https://huggingface.co/"+model_id+" then restart your worker.")
                torch.cuda.empty_cache()
                gc.collect()
                print("Reverting to ", previous_model_id)
                pipe = build_pipeline(previous_model_id, revision=revision, use_auth_token=os.environ["HF_AUTH_TOKEN"], vae=AutoencoderKL.from_pretrained(vae))
                model_id = previous_model_id

            if enable_attention_slicing:
                pipe.enable_attention_slicing()
                torch.cuda.empty_cache()
                gc.collect()

            if enable_xformers_memory_efficient_attention:
                pipe.enable_xformers_memory_efficient_attention()
            pipe = pipe.to(device)
            scheduler_config = pipe.scheduler.config
            scheduler_cache = {}
            print("Model switched: " + model_id)
            continue
        cipher = detail['prompt_cipher']
        seed = detail['seeds'].pop(0)
        guidance_scale = detail['guidance_scale']
        num_inference_steps = detail['num_inference_steps']
        self_attention_guidance_scale = None
        if "self_attention_guidance_scale" in detail:
            self_attention_guidance_scale = detail['self_attention_guidance_scale']
        precision_scope = torch.autocast
        negative_prompt = [detail['negative_prompt']]

        precision_scope = torch.autocast
        with torch.no_grad():
            generator = torch.Generator(device=device).manual_seed(seed)
            height = detail['height']
            width = detail['width']

            if detail['op'] == 'txt2img':
                if model_type == "versatile":
                    in_channels = pipe.image_unet.in_channels
                else:
                    in_channels = pipe.unet.in_channels
                latents = torch.randn(
                    (1, in_channels, height // 8, width // 8),
                    generator = generator,
                    device = device
                )
                while(len(detail['variants']) > 0):
                    seed = detail['seeds'].pop(0)
                    variant = float(detail['variants'].pop(0))
                    generator = torch.Generator(device=device).manual_seed(seed)
                    latents2 = torch.randn(
                            (1, in_channels, height // 8, width // 8),
                            generator = generator,
                            device = device
                    )
                    latents2 *= variant
                    s1 = torch.std(latents)
                    s2 = torch.std(latents2)
                    ns = torch.sqrt(s1**2+s2**2)
                    latents += latents2
                    latents -= latents2.mean()
                    latents *= s1/ns

            if model_type != "upscale":
                pipe.scheduler = schedulers.get_scheduler(detail['sampler'], scheduler_config, scheduler_cache)

            # friendly reminder that spying on user I/O is against TOS
            prompts = [codecs.encode(base64.b64decode(cipher.encode()).decode("utf-8"), "rot13")]
            if self_attention_guidance_scale is not None:
                if enable_xformers_memory_efficient_attention:
                    pipe.disable_xformers_memory_efficient_attention()
                if enable_attention_slicing:
                    pipe.disable_attention_slicing()
                    torch.cuda.empty_cache()
                    gc.collect()

            if detail['op'] == 'txt2img':
                with precision_scope("cuda"):
                    if model_type == "alt": #TODO expose embeddings
                        output = pipe(prompts, width=width, height=height, latents = latents, guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, negative_prompt=negative_prompt)
                    elif model_type == "versatile": #TODO expose embeddings
                        output = pipe.text_to_image(prompts, width=width, height=height, latents = latents, guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, negative_prompt=negative_prompt)
                    else:
                        output, embeddings = pipe(prompts, width=width, height=height, self_attention_guidance_scale = self_attention_guidance_scale, latents = latents, guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, negative_prompt=negative_prompt)
            elif detail['op'] == 'img2img':
                with precision_scope("cuda"):
                    try:
                        init_image = PIL.Image.open(requests.get(detail["init_image"], stream=True).raw).convert("RGB").resize((width, height))
                    except(requests.exceptions.ConnectionError, PIL.UnidentifiedImageError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                        requests.post(host+"/runs/"+str(detail["run_id"])+"/error", data={"message":"Invalid init_image or mask_image"}, headers=headers, timeout=timeout)
                        break

                    output = pipe.img2img(prompts, width=width, height=height, image=init_image, strength=detail['strength'], guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, negative_prompt=negative_prompt, generator=generator)
                    embeddings = None
            elif detail['op'] == 'inpainting':
                with precision_scope("cuda"):
                    try:
                        init_image = PIL.Image.open(requests.get(detail["init_image"], stream=True).raw).convert("RGB").resize((width, height))
                        mask_image = PIL.Image.open(requests.get(detail["mask_image"], stream=True).raw).convert("RGB").resize((width, height))
                    except(requests.exceptions.ConnectionError, PIL.UnidentifiedImageError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                        requests.post(host+"/runs/"+str(detail["run_id"])+"/error", data={"message":"Invalid init_image or mask_image"}, headers=headers, timeout=timeout)
                        break

                    output = pipe.inpaint(prompts, width=width, height=height, image=init_image, mask_image=mask_image, guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, negative_prompt=negative_prompt, generator=generator, output_type="np.array")
                    output.images = pipe.numpy_to_pil(output.images)
                    embeddings = None
            elif detail['op'] == 'dual_guided':
                with precision_scope("cuda"):
                    try:
                        init_image = PIL.Image.open(requests.get(detail["init_image"], stream=True).raw).convert("RGB").resize((width, height))
                    except(requests.exceptions.ConnectionError, PIL.UnidentifiedImageError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                        requests.post(host+"/runs/"+str(detail["run_id"])+"/error", data={"message":"Invalid init_image or mask_image"}, headers=headers, timeout=timeout)
                        break

                    output = pipe.dual_guided(prompts, init_image, width=width, height=height, text_to_image_strength=detail['strength'], guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, generator=generator)
                    embeddings = None
            elif detail['op'] == 'image_variation':
                with precision_scope("cuda"):
                    try:
                        init_image = PIL.Image.open(requests.get(detail["init_image"], stream=True).raw).convert("RGB").resize((width, height))
                    except(requests.exceptions.ConnectionError, PIL.UnidentifiedImageError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                        requests.post(host+"/runs/"+str(detail["run_id"])+"/error", data={"message":"Invalid init_image or mask_image"}, headers=headers, timeout=timeout)
                        break

                    output = pipe.image_variation(init_image, width=width, height=height, guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, generator=generator)
                    embeddings = None
            elif detail['op'] == 'upscale':
                try:
                    init_image = PIL.Image.open(requests.get(detail["init_image"], stream=True).raw).convert("RGB").resize((width, height))
                except(requests.exceptions.ConnectionError, PIL.UnidentifiedImageError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                    requests.post(host+"/runs/"+str(detail["run_id"])+"/error", data={"message":"Invalid init_image or mask_image"}, headers=headers, timeout=timeout)
                    break
                output = pipe(prompt=prompts, image=init_image, noise_level=detail["noise_level"], guidance_scale=guidance_scale, num_inference_steps=num_inference_steps, negative_prompt=negative_prompt)
                embeddings = None
            else:
                print("Error: Op not supported '"+str(detail['op'])+"'")
                break

            if model_type != "upscale":
                pipe.scheduler = None
            if self_attention_guidance_scale is not None:
                if enable_xformers_memory_efficient_attention:
                    pipe.enable_xformers_memory_efficient_attention()
                if enable_attention_slicing:
                    pipe.enable_attention_slicing()
                    torch.cuda.empty_cache()
                    gc.collect()

        if 'embeddings_upload_url' in detail and detail['embeddings_upload_url'] is not None and embeddings is not None:
            efileio = io.BytesIO()
            torch.save(embeddings, efileio)
            efileio.seek(0)
            while(True):
                try:
                    r = requests.put(detail['embeddings_upload_url'], data=efileio, timeout=timeout)
                except(requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                    print("Error connecting to image embedding upload, retrying in 5 seconds")
                    time.sleep(5)
                    continue
                break

        fileio = io.BytesIO()
        image_format = 'PNG'
        output.images[0].save(fileio, image_format)
        fileio.seek(0)
        while(True):
            try:
                r = requests.put(detail['upload_url'], data=fileio, timeout=timeout)
            except(requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                print("Error connecting to image upload, retrying in 5 seconds")
                time.sleep(5)
                continue
            break
        while(True):
            try:
                r = requests.post(host+"/runs/"+str(detail["run_id"])+"/complete", headers=headers, timeout=timeout)
            except(requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, requests.urllib3.exceptions.NewConnectionError, requests.urllib3.exceptions.TimeoutError):
                print("Error connecting to xno.ai, retrying in 5 seconds")
                time.sleep(5)
                continue
            break
